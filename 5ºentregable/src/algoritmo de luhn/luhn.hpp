/*
 * algoritmodeluhn.hpp
 *
 *  Created on: 11 abr. 2018
 *      Author: PCA
 */

#ifndef ALGORITMO_DE_LUHN_LUHN_HPP_
#define ALGORITMO_DE_LUHN_LUHN_HPP_
#include <iostream>
using namespace std;

int luhn(string numtarjeta);
void luhnprueba();

#endif /* ALGORITMO_DE_LUHN_LUHN_HPP_ */
